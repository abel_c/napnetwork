//
//  NetworkTests.swift
//  NetworkTests
//
//  Created by Abel Castro on 24/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import XCTest
import NAPCore
@testable import NAPNetwork

class NetworkTests: XCTestCase {
  
  fileprivate let key = "favouritesKey"
  fileprivate var persistItemManager: NAPPersistItemManagerProtocol!
  
  override func setUp() {
    super.setUp()
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
    persistItemManager = NAPPersistItemManager.sharedInstance
  }
  
  override func tearDown() {
    super.tearDown()
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
  }
  
  func test_addFavourite() {
    //Given
    XCTAssertNil(UserDefaults.standard.array(forKey: key))
    let item = NAPCItem.get()
    
    // When
    persistItemManager.addFavourite(item: item)
    persistItemManager.saveFavourites()
    // Then
    let favourites = UserDefaults.standard.array(forKey: key) as? [Int]
    XCTAssertNotNil(favourites)
    if let favourites = favourites {
      XCTAssertEqual(favourites.first, item.id)
    }
    persistItemManager.removeFavourite(item: item)

  }
  
  func test_removeFavourite() {
    //Given
    XCTAssertNil(UserDefaults.standard.array(forKey: key))
    let item = NAPCItem.get()
    persistItemManager.addFavourite(item: item)
    persistItemManager.saveFavourites()
    var favourites = UserDefaults.standard.array(forKey: key) as? [Int]
    XCTAssertNotNil(favourites)
    
    if let favourites = favourites {
      XCTAssertTrue(favourites.contains(item.id))
      XCTAssertEqual(favourites.first, item.id)
    }
    // When
    persistItemManager.removeFavourite(item: item)
    persistItemManager.saveFavourites()
    
    // Then
    favourites = UserDefaults.standard.array(forKey: key) as? [Int]
    XCTAssertNotNil(favourites)
    if let favourites = favourites {
      XCTAssertFalse(favourites.contains(item.id))
      XCTAssertNotEqual(favourites.first, item.id)
    }
  }
  
  func test_isFavourite() {
    //Given
    let favouriteItem = NAPCItem.get()
    let notFavouriteItem = NAPCItem.get(id: 000)

    // When
    persistItemManager.addFavourite(item: favouriteItem)
    
    // Then
    XCTAssertTrue(persistItemManager.isFavourite(item: favouriteItem), "Item should be favourite")
    XCTAssertFalse(persistItemManager.isFavourite(item: notFavouriteItem), "Item should not be favourite")
    
    // And When
    persistItemManager.removeFavourite(item: favouriteItem)

    // Then
    XCTAssertFalse(persistItemManager.isFavourite(item: favouriteItem), "Item should not be favourite")
  }
  
}
