//
//  Mocks.swift
//  NAPNetworkTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import XCTest
@testable import NAPNetwork

class NAPCoreAPIMock : NAPCoreAPIOffline {
  var expectation: XCTestExpectation?
  override func downloadImage(url: URL, completionHandler: @escaping ((UIImage?)->())) {
    super.downloadImage(url: url) { (image) in
      completionHandler(image)
    }
    DispatchQueue.global().asyncAfter(deadline: .now() + 3.0) { [weak self] in
      self?.expectation?.fulfill()
    }
  }
}
