//
//  NAPNetworkErrorTests.swift
//  NAPNetworkTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPNetwork

class NAPNetworkErrorTests: XCTestCase {
 
    func testCustomError() {
      // Given
      let description = "Error description"
      
      // When
      let error = NAPNetworkError.customError(description: description)
      
      // Then
      XCTAssertEqual(error.domain, "NAPNetwork")
      XCTAssertEqual(error.errorDescription, description)
    }
    
  
    
}
