//
//  NAPImageCacheManagerTests.swift
//  NAPNetworkTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
import NAPCore
@testable import NAPNetwork

class NAPImageCacheManagerTests: XCTestCase {
  
  fileprivate var apiMock: NAPCoreAPIMock!
  fileprivate var manager: NAPImageCacheManagerProtocol!
  
  override func setUp() {
    super.setUp()
    apiMock = NAPCoreAPIMock()
    manager = NAPImageCacheManager(api: apiMock)
  }
  
  func testGetImage() {
    // Given
    apiMock.expectation = expectation(description: "download_image")
    let item = NAPCItem.get()

    // When
    self.manager.getImage(for: item) {  (image) in
      // Then
      XCTAssertNotNil(image, "Image should not be nil")
    }
    self.waitForExpectations(timeout: 10.0) { [weak self] (error) in
    
    // And when
    self?.manager.getImage(for: item) {  (image) in
        // Then
        XCTAssertNotNil(image, "Image should not be nil")
      }
    }
  }
  
  func testFlushCache() {
    // Given
    XCTAssertTrue(directoryExists(), "Images directories should exist")
    // When
    manager.flushCache()
    // Then
    XCTAssertFalse(directoryExists(), "Images directories should exist")
  }
  
  fileprivate func directoryExists() -> Bool {
    guard let docDir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else { return false }
    let imagesURL = docDir.appendingPathComponent("/Images")
    var isDir: ObjCBool = true
    return FileManager.default.fileExists(atPath: imagesURL.path, isDirectory: &isDir)

  }
}
