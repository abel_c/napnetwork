//
//  NAPCoreAPI.swift
//  NAPNetwork
//
//  Created by Abel Castro on 12/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

protocol NAPCoreAPIProtocol {
  func downloadImage(url: URL, completionHandler: @escaping ((UIImage?)->()))
}
class NAPCoreAPI : NAPCoreAPIProtocol {
  func downloadImage(url: URL, completionHandler: @escaping ((UIImage?)->())) {
    NAPNetworkManager.sharedInstance.downloadImage(url: url, completionHandler: completionHandler)
  }
}

