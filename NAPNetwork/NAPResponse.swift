//
//  NAPResponse.swift
//  NAPNetwork
//
//  Created by Abel Castro on 25/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation

public protocol NAPResponseProtocol {
  static func responseObject(data: [String:Any]) -> Self
}

public enum Result<Resp: NAPResponseProtocol> {
  case success(value: Resp)
  case failure(error: Error)
}

public enum ResultStat : String {
  case ok
  case fail
}
