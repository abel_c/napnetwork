//
//  Commons.swift
//  TSNetwork
//
//  Created by Abel Castro on 29/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation

// For this demonstration, I didn't go too far but in practice, this class would go in another separate Framework (Core) with all commmon operations for the other Frameworks
public struct Delayer {

  public static func delay(_ operation: @escaping () ->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      operation()
    }
  }
}
