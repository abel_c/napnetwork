//
//  NAPCoreAPIFactory.swift
//  NAPNetwork
//
//  Created by Abel Castro on 12/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

class NAPCoreAPIFactory {
  func getAPI() -> NAPCoreAPIProtocol {
    #if OFFLINE
      return NAPCoreAPIOffline()
    #else
      return NAPCoreAPI()
    #endif
  }
}
