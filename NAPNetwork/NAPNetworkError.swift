//
//  NAPNetworkError.swift
//  NAPNetwork
//
//  Created by Abel Castro on 29/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation

enum NAPNetworkError: Error {
  case customError(description: String)
}

extension NAPNetworkError: LocalizedError {
  var domain: String {
    return "NAPNetwork"
  }
  var errorDescription: String? {
    switch self {
    case .customError(let description):
      return NSLocalizedString(description, comment: "Error description")
    }
  }
}
