//
//  NAPCoreAPIOffline.swift
//  NAPNetwork
//
//  Created by Abel Castro on 12/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation

class NAPCoreAPIOffline : NAPCoreAPIProtocol {
  func downloadImage(url: URL, completionHandler: @escaping ((UIImage?)->())) {
    Delayer.delay {
      let image: UIImage? = UIImage(named: "Image-not-available", in: Bundle(for: NAPNetworkManager.self), compatibleWith: nil)
      completionHandler(image)
    }
  }
}
