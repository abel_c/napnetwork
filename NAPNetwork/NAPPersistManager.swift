//
//  NAPPersistManager.swift
//  NAPNetwork
//
//  Created by Abel Castro on 13/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPCore

public protocol NAPPersistItemManagerProtocol {
  static var sharedInstance: NAPPersistItemManagerProtocol {get}
  func removeFavourite(item: NAPCItemProtocol)
  func addFavourite(item: NAPCItemProtocol)
  func isFavourite(item: NAPCItemProtocol) -> Bool
  func saveFavourites()
}

public class NAPPersistItemManager : NAPPersistItemManagerProtocol {
  
  public static var sharedInstance: NAPPersistItemManagerProtocol = NAPPersistItemManager()
  fileprivate let favouritesKey = "favouritesKey"
  internal var favourites: [Int] = []
  
  fileprivate init() {
    favourites = loadFavourites()
  }
  public func addFavourite(item: NAPCItemProtocol) {
    favourites.append(item.id)
  }
  public func removeFavourite(item: NAPCItemProtocol) {
    if let index = favourites.index(of: item.id) {
      favourites.remove(at: index)
    }
  }
  
  public func isFavourite(item: NAPCItemProtocol) -> Bool {
    return favourites.contains(item.id)
  }
  
  public func saveFavourites() {
    UserDefaults.standard.set(favourites, forKey: favouritesKey)
    UserDefaults.standard.synchronize()
  }
  
  
  fileprivate func loadFavourites() -> [Int] {
    guard let favourites = UserDefaults.standard.array(forKey: favouritesKey) as? [Int] else {
      return []
    }
    return favourites
  }
}
