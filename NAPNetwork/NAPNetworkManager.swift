//
//  NAPNetworkManager.swift
//  Network
//
//  Created by Abel Castro on 24/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

public class NAPNetworkManager {
  fileprivate static let kSerializeError = 777
  
  // Uses a singleton, because there should be only one instacen capable of doing network request
  // It is customizable and settable for this Net-a-porter BackEnd
  // You set an endpoint, common headers, etc
  // Its also a facade because it hides all the logic underneath and only exposes, a few (public) methods, to do the most important operations
  public static var sharedInstance = NAPNetworkManager()
  public var defaultEndpoint: String?
  public var itemsPerPage: UInt = 100
  public var offset: Int = 0

  fileprivate let sessionManager: SessionManager
  
  fileprivate struct Keys {
  }
  
  var headers: HTTPHeaders = [:]
  var parameters: [String:Any] {
    return [ :
    ]
  }
  
  fileprivate init() {
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 60 // seconds
    configuration.timeoutIntervalForResource = 20
    sessionManager = Alamofire.SessionManager(configuration: configuration)
  }
  // Uses generics to parse all differenet responses from the server into Generic Responses objects/
  public func request<Resp>(path: String, params: [String:Any]? = nil, completionHandler: @escaping ((Result<Resp>)->())) {
    guard let defaultEndpoint = self.defaultEndpoint else {
      assert(false, "Default endpoint should be set")
      return
    }
    let endpoint = defaultEndpoint.appending("/\(itemsPerPage)/\(offset)/\(path)")
    var finalParams: Parameters = parameters
    if let params = params {
      finalParams = finalParams.merging(params) { return $1 }
    }
    
    sessionManager.request(endpoint, method: HTTPMethod.get, parameters: finalParams, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
      let resp: Result<Resp>
      switch response.result {
      case .success(let data):
        if let dataDict = data as? [String:Any] {          
          if let serialized = NAPOutputSerializer<Resp>().serialize(data: dataDict) {
            resp = Result<Resp>.success(value: serialized)
          }
          else {
            resp = Result<Resp>.failure(error: NAPOutputSerializer<Resp>().error(data: dataDict))
          }
        }
        else {
          let error = NAPNetworkError.customError(description: NSLocalizedString("Couldn't complete operation", comment: "error from server"))
          resp = .failure(error: error)
        }
      case .failure(let error):
        resp = .failure(error: error)
      }
      completionHandler(resp)
    }
  }
  
  public func downloadImage(url: URL, completionHandler: @escaping ((UIImage?)->())) {
    
    sessionManager.download(url) { (url, _) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
      var image: UIImage? = nil
      if let data = try? Data(contentsOf: url), let _image = UIImage(data: data) {
        image = _image
      }
      
      completionHandler(image)
      return (url, [])
      
      }.response { (response) in
        if response.destinationURL == nil {
          completionHandler(nil)
        }
    }
  }
}
