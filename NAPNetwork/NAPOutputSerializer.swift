//
//  NAPOutputSerializer.swift
//  NAPNetwork
//
//  Created by Abel Castro on 25/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation

class NAPOutputSerializer<Resp: NAPResponseProtocol>  {
  
  fileprivate struct Keys {
    let error = "message"
  }
  fileprivate let keys = Keys()
  
  func serialize(data: [String:Any]) -> Resp? {
    guard data.count > 0 else {
      return nil
    }
    return Resp.responseObject(data: data)
  }
  
  func error(data: [String:Any]) -> Error {
    let message = data[keys.error] as? String ?? "Generic error"
    return NAPNetworkError.customError(description: message)
  }
}
