//
//  NAPReachability.swift
//  NAPNetwork
//
//  Created by Abel Castro on 25/12/17.
//  Copyright © 2017 Abel Castro. All rights reserved.
//

import Foundation
import Alamofire

// Observer pattern
// Any observable can subscript to the Observer and get notified when a change occurs
struct ListenerWrapper {
  weak var listener: NAPReachabilityListenerProtocol?
}
public protocol NAPReachabilityListenerProtocol : class {
  func notify(message: String, positive: Bool)
}

public class NAPReachability {
  fileprivate enum Status {
    case reachable, notReachable
  }
  public static let sharedInstance = NAPReachability()
  
  fileprivate var previousStatus: NAPReachability.Status?
  fileprivate var listeners: [ListenerWrapper] = []
  fileprivate let reachabilityManager = NetworkReachabilityManager()
  
  init() {
    reachabilityManager?.listener = { [weak self] status in
      
      let message: String
      let currentStatus: NAPReachability.Status
      
      switch status {
      case .reachable:
        message = NSLocalizedString("You internet connection is back up", comment: "User has internet connection")
        currentStatus = .reachable
      default:
        message = NSLocalizedString("You don't have connectivity right now", comment: "User doesn't have internet connection")
        currentStatus = .notReachable
      }
      if let previous = self?.previousStatus, previous != currentStatus {
        self?.listeners.forEach({ $0.listener?.notify(message: message, positive: currentStatus == .reachable) })
      }
      
      self?.previousStatus = currentStatus
    }
    reachabilityManager?.startListening()
  }
  
  public func add(listener: NAPReachabilityListenerProtocol) {
    self.listeners.append(ListenerWrapper(listener: listener))
  }
  
  deinit {
    self.listeners.removeAll()
  }
}
