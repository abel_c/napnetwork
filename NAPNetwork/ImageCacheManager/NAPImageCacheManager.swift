//
//  NAPImageCacheManager.swift
//  NAPNetwork
//
//  Created by Abel Castro on 12/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPCore

public protocol NAPImageCacheManagerProtocol {
  func getImage(for item: NAPCItemProtocol, completionHandler: @escaping (UIImage?) ->())
  func flushCache()
}

public class NAPImageCacheManager : NAPImageCacheManagerProtocol {
  
  var api: NAPCoreAPIProtocol = NAPCoreAPIFactory().getAPI()
  let images = "Images"
  var imagesURL: URL
  
  convenience internal init(api: NAPCoreAPIProtocol = NAPCoreAPIFactory().getAPI()) {
    self.init()
    self.api = api
  }
  public init() {
      var docDir: URL?
      do {
        docDir = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        guard let docDir = docDir else {
          imagesURL = URL(string: "/")!
          return }
        imagesURL = docDir.appendingPathComponent(images)
        try FileManager.default.createDirectory(at: imagesURL, withIntermediateDirectories: true, attributes: nil)
    } catch {
      // handle the error
      print(error.localizedDescription)
      imagesURL = URL(string: "/")!
    }

  }
  
  public func getImage(for item: NAPCItemProtocol, completionHandler: @escaping (UIImage?) ->()) {
    if isImageSaved(item: item) {
     completionHandler(retrieveImage(item: item))
    }
    else {
      downloadImage(for: item, completionHandler: completionHandler)
    }
  }
  
  fileprivate func isImageSaved(item: NAPCItemProtocol) -> Bool {
    return FileManager.default.fileExists(atPath: imagesURL.appendingPathComponent(String(describing: item.id)).path)
  }
  
  fileprivate func retrieveImage(item: NAPCItemProtocol) -> UIImage? {
    guard let data = FileManager.default.contents(atPath: imagesURL.appendingPathComponent(String(describing: item.id)).path) else { return nil }
    return UIImage(data: data)
  }
  
  
  fileprivate func downloadImage(for item: NAPCItemProtocol, completionHandler: @escaping (UIImage?) ->()) {
    guard let url = item.images.imageURL() else { return }
    
    api.downloadImage(url: url) { [weak self] (image) in
      if let image = image {
        DispatchQueue.global().async {
          self?.save(image: image, for: item)
        }
      }
      completionHandler(image)
    }
  }
  
  fileprivate func save(image: UIImage, for item: NAPCItemProtocol) {
    var data: Data? = nil
    guard let url = item.images.imageURL() else { return }
    switch url.absoluteString.imageExtension() {
    case "jpg":
      data = UIImageJPEGRepresentation(image, 1)
    default:
      data = UIImagePNGRepresentation(image)
    }
    FileManager.default.createFile(atPath: imagesURL.appendingPathComponent(String(describing: item.id)).path, contents: data, attributes: nil)
  }
  
  public func flushCache() {
    do {
      try FileManager.default.removeItem(at: imagesURL)
    }
    catch {
      print(error.localizedDescription)
    }
  }
  
}

fileprivate extension String {
  fileprivate func imageExtension() -> String {
    
    var imageExtension: String = ""
    if let ext: String = self.components(separatedBy: ".").last {
      switch ext {
      case "png":
        imageExtension = "png"
      case "jpg":
        imageExtension = "jpg"
      default: break
      }
    }
    return imageExtension
  }
}
